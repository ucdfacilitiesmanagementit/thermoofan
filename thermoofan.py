#####################################
#Flask and Flask-SocketIO Imports
from flask import Flask, render_template
from flask_socketio import SocketIO, emit

#####################################
#LabJack Import
import LJTickDAC as lj

#####################################
# Threading import for running functions at set intervals
from threading import Thread

#####################################
# Sys import for command line parameters to set dev variable
import sys

dev = False
for value in sys.argv:
    if value == '--dev':
        dev = True
        break

#####################################
#Logging
import logging
logging.basicConfig(level=logging.DEBUG, filename='fan.log')

#####################################
#Datetime Utilities
import datetime as dt
import dateutil.parser  #for parsing ISO 8601 timestamps from Thermoostat API
import calendar
todaysDate = None

#####################################
#SQLite Import and variables
import sqlite3
sqlite_file = 'thermoofan_db.sqlite'
db_table = 'giedt1001'
field1name = 'timeStamp'
field1type = 'TEXT' # e.g. YYY-MM-DD HH:MM:SS.SSS
field2name = 'fanVolts'
field2type = 'REAL' # e.g. 2.5
field3name = 'manualOverride'
field3type = 'INTEGER' # 0 for false, 1 for true

#####################################
#urllib2 & json libraries for parsing Thermoostat API in thermoostat_query function
import urllib2
import json
opener = urllib2.build_opener()
import ssl

#####################################
#Schedule Utilities
import schedule
import time


defaultFanVolts = 1.415
currentState = ''
manualOverride = 0  #0 is false, 1 is true; for osiSoft PI tag

def setUnoccupied():
    global currentState
    global manualOverride

    currentState = 'unoccupied'
    manualOverride = 0
    slider_v.volt = 0
    update_lj(labjack)
    emit_change(0)      #reset iPad interface back to 0 and make no voltage changes
    insert_row()
    printStatus()

def setOccupiedDefault():
    global todaysExperimentSchedule
    global currentState
    global manualOverride
    global defaultFanVolts

    schedule.clear('check-feedbacks')

    if todaysExperimentSchedule == 'baseline':
        setUnoccupied()
    else:
        currentState = 'occupiedDefault'

        if todaysExperimentSchedule == 'treatment2':
            schedule.every().minute.do(check_feedbacks).tag('check-feedbacks')

        manualOverride = 0
        slider_v.volt = defaultFanVolts
        update_lj(labjack)
        emit_change(slider_v.volt*10)
        insert_row()
        printStatus()

def setOccupiedOverridden(value):
    global todaysExperimentSchedule
    global currentState
    global manualOverride

    schedule.clear('check-feedbacks')

    if todaysExperimentSchedule == 'baseline':
        setUnoccupied()
    else:
        currentState = 'occupiedOverriden'
        manualOverride = 1
        slider_v.volt = value/10
        update_lj(labjack)
        emit_change(value)
        insert_row()
        printStatus()

def setOccupiedWithVotes(volts):
    global currentState
    global manualOverride
    global todaysExperimentSchedule

    if todaysExperimentSchedule == 'baseline':
        setUnoccupied()
    else:
        currentState = 'occupiedWithVotes'
        manualOverride = 0
        slider_v.volt = volts
        update_lj(labjack)
        emit_change(volts*10)
        insert_row()
        printStatus()

todaysExperimentSchedule = ''

baselineDates = frozenset([
    '01-23-2018',
    '01-25-2018',
    '02-06-2018',
    '02-08-2018',
    '02-20-2018',
    '02-22-2018'
])

treatment1Dates = frozenset([
    '01-15-2018',
    '01-16-2018',
    '01-17-2018',
    '01-18-2018',
    '01-19-2018',
    '01-22-2018',
    '01-24-2018',
    '01-26-2018',
    '01-29-2018',
    '01-30-2018',
    '01-31-2018',
    '02-01-2018',
    '02-02-2018',
    '02-05-2018',
    '02-07-2018',
    '02-09-2018',
    '02-13-2018',
    '02-15-2018'
])


def checkExperimentSchedule():
    todaysDate = dt.datetime.today().strftime("%m-%d-%Y")
    todaysDay = calendar.day_name[dt.datetime.today().weekday()]

    global todaysExperimentSchedule

    if todaysDay == 'Saturday' or todaysDay == 'Sunday':
        todaysExperimentSchedule = 'baseline'
    else:
        if todaysDate in baselineDates:
            todaysExperimentSchedule = 'baseline'
        elif todaysDate in treatment1Dates:
            todaysExperimentSchedule = 'treatment1'
        else:    #treatment2
            todaysExperimentSchedule = 'treatment2'

#####################################
#Class / variables to track slider voltage
class slider_voltage:
    def __init__(self):
        self.volt = 0

slider_v = slider_voltage()
labjack = lj.LJTickDAC()


#####################################
# function to return JSON of today's data from Thermoostat API
def thermoostat_today_query():
    apiKey = 'ET9wgnDTT546jGaN'
    todaysDate = dt.datetime.today().strftime("%m-%d-%Y")

    baseUrl = 'https://thermoostat.ucdavis.edu/api/thermoo/'
    if dev == True:
        baseUrl = 'http://dev-thermoostat.ucdavis.edu/api/thermoo/'

    req = urllib2.Request(baseUrl+apiKey+"/date/"+todaysDate)
    f = opener.open(req)
    jsonFile = json.loads(f.read())
    return jsonFile
    opener.close()

#####################################
# function to parse thermoostat query Giedt 1001 results
def parse_out_giedt_1001(query):
    giedtBuildingFeedbacks = []
    giedtRoom1001Feedbacks = []
    currentlyHereFeedbacks = []

    #parse out Giedt Hall feedbacks
    for obj in query:
        for key, value in obj.items():
            if key == 'building' and value == 'Giedt Hall':
                    giedtBuildingFeedbacks.append(obj)

    #parse out Giedt Hall room 1001 feedbacks
    for obj in giedtBuildingFeedbacks:
        for key, value in obj.items():
            if key == 'room' and value == '1001':
                    giedtRoom1001Feedbacks.append(obj)

    #parse out any feedbacks where currentlyHere = true (which, counterintuitively, means they are NOT currently here)
    for obj in giedtRoom1001Feedbacks:
        for key, value in obj.items():
            if key == 'notCurrentlyHere' and value == False:
                    currentlyHereFeedbacks.append(obj)

    return currentlyHereFeedbacks

#####################################
# function to parse array of feedback objects for just today's feedbacks
def parse_out_offset(feedbacks):
    offsetIntervalFeedbacks = []

    offsetIntervalMinutes = 1  # Last X Minutes to check from TherMOOstat API

    datetimeNow = dt.datetime.now(dateutil.tz.tzutc())    #today's datetime in UTC timezone to match thermoostat api
    datetimeNowMinusOffset = datetimeNow - dt.timedelta(minutes=offsetIntervalMinutes)

    #parse out Giedt Hall room 1001 feedbacks from the last 1 minute
    for obj in feedbacks:
        for key, value in obj.items():
            if key == 'timestamp':
                datetime = dateutil.parser.parse(value)
                if datetime > datetimeNowMinusOffset:
                    offsetIntervalFeedbacks.append(obj)

    return offsetIntervalFeedbacks

#####################################
# function to only capture the most recent feedback from each user, in case they try to vote 2 or more times within a single interval
def parse_out_latest_from_each_user(feedbacks):
    latestFeedbacksFromEachUser = []
    usernames = []

    for obj in reversed(feedbacks):     #reversed() function allows us to traverse backward through the array, since the items in the array will always be in chronological order
        for key, value in obj.items():
            if key == 'username':
                username = value
                if not username in usernames:
                    usernames.append(username)
                    latestFeedbacksFromEachUser.append(obj)

    return latestFeedbacksFromEachUser

def find_delta_v(feedbacks):
    coldVotes = 0
    chillyVotes = 0
    perfectVotes = 0
    warmVotes = 0
    hotVotes = 0

    coldWeight = -1.015
    chillyWeight = -0.5075
    perfectWeight = 0
    warmWeight = 0.5075
    hotWeight = 1.015

    deltaV = 0.0

    for obj in feedbacks:
        for key, value in obj.items():
            if key == 'comfort':
                vote = value
                if vote == 'cold':
                    coldVotes += 1
                elif vote == 'chilly':
                    chillyVotes += 1
                elif vote == 'perfect':
                    perfectVotes += 1
                elif vote == 'warm':
                    warmVotes += 1
                elif vote == 'hot':
                    hotVotes += 1

    if coldVotes > 0:
        print "Cold Votes: " + str(coldVotes)
        logging.info("Cold Votes: " + str(coldVotes))
    if chillyVotes > 0:
        print "Chilly Votes: " + str(chillyVotes)
        logging.info("Chilly Votes: " + str(chillyVotes))
    if perfectVotes > 0:
        print "Perfect Votes: " + str(perfectVotes)
        logging.info("Perfect Votes: " + str(perfectVotes))
    if warmVotes > 0:
        print "Warm Votes: " + str(warmVotes)
        logging.info("Warm Votes: " + str(warmVotes))
    if hotVotes > 0:
        print "Hot Votes: " + str(hotVotes)
        logging.info("Hot Votes: " + str(hotVotes))

    totalVotes = coldVotes + chillyVotes + perfectVotes + warmVotes + hotVotes

    print "Total Votes: " + str(totalVotes)
    logging.info("Total Votes: " + str(totalVotes))

    deltaV = ((coldVotes*coldWeight)+(chillyVotes*chillyWeight)+(perfectVotes*perfectWeight)+(warmVotes*warmWeight)+(hotVotes*hotWeight)) / totalVotes

    print "DeltaV: " + str(deltaV) + " volts"
    logging.info("DeltaV: " + str(deltaV) + " volts")

    return deltaV


def check_feedbacks():
    jsonQuery = thermoostat_today_query()
    giedt1001 = parse_out_giedt_1001(jsonQuery)
    lastMinuteOfFeedbacks = parse_out_offset(giedt1001)

    maxVolts = 2.115
    minVolts = 1.1

    if lastMinuteOfFeedbacks:   #if array is not empty, so there are feedbacks in the last minute
        latestFeedbacks = parse_out_latest_from_each_user(lastMinuteOfFeedbacks)
        print "Last minute of feedbacks: "
        print latestFeedbacks
        logging.info("Last minute of feedbacks: ")
        logging.info(latestFeedbacks)

        deltaV = find_delta_v(latestFeedbacks)
        newV = slider_v.volt + deltaV
        if newV > maxVolts:
            newV = maxVolts
        elif newV < minVolts:
            newV = minVolts

        setOccupiedWithVotes(newV)


def printStatus():
    global todaysExperimentSchedule
    global currentState

    timestamp = '{:%Y-%m-%d %H:%M:%S}'.format(dt.datetime.now())
    print "Current timestamp is: "+timestamp
    print "Today's experiment schedule is: "+todaysExperimentSchedule
    print "Current state is: "+currentState
    print "Current voltage is: %.2f VOLTS" % slider_v.volt
    print " "

    logging.info("Current timestamp is: "+timestamp)
    logging.info("Today's experiment schedule is: "+todaysExperimentSchedule)
    logging.info("Current state is: "+currentState)
    logging.info("Current voltage is: %.2f VOLTS" % slider_v.volt)
    logging.info(" ")

#####################################
#initialize Flask webserver and Websocket layer
app = Flask(__name__)
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)


#####################################
#Values object to track slider value
values = {
    'value1': 0
}

def emit_change(value):
    message = {
        'who': 'value1',
        'data': value
    }
    # socketio.emit('update value', message, broadcast=True)
    socketio.emit('update value', message, broadcast=True)

#####################################
#update_lj function to change LabJack voltage and send new voltage back to browser
def update_lj(labjack):
    if slider_v.volt != labjack.VOLT_A:
        labjack.VOLT_A = slider_v.volt
        labjack.VOLT_B = slider_v.volt
        labjack.updateDevice()


#####################################
#Event listner for socket event "value changed" sent from browser
@socketio.on('value changed')
def value_changed(message):
    global currentState
    global manualOverride

    value = float(message['data'])
    if currentState == 'unoccupied':
        #just emit 0 so UI resets to "off"
        emit_change(0)
    else:
        setOccupiedOverridden(value)

#####################################
#On client connect, send the current value
@socketio.on('connect')
def new_connect():
    emit_change(slider_v.volt*10)

#####################################
#Database row insert function for local sqlite backup database
def insert_row():
    global manualOverride
    timestamp = '{:%Y-%m-%d %H:%M:%S}'.format(dt.datetime.now())

    # Connecting to the database file
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    # Insert the row
    c.execute("INSERT INTO giedt1001 VALUES (?,?,?)", (timestamp, slider_v.volt, manualOverride))
    # Commit changes and close the connection
    conn.commit()
    conn.close()
    # send_to_pi_ufl_connector(timestamp)  #send data to UFL Connector every time we insert a row in the local database, with the same timestamp so they match

def send_to_pi_ufl_connector(timestamp):
    global manualOverride

    print "Sending new fan speed to PI UFL Connector..."
    logging.info("Sending new fan speed to PI UFL Connector...")

    ssl._create_default_https_context = ssl._create_unverified_context

    opener = urllib2.build_opener(urllib2.HTTPHandler)
    request = urllib2.Request('https://util-cs-iis.ou.ad3.ucdavis.edu:5460/connectordata/GiedtFan/', data=timestamp+', Giedt1001-Ceiling-Fan, '+str(slider_v.volt)+', '+str(manualOverride))
    request.add_header('Authorization', 'Basic dWZsY29ubmVjdG9yOmNRJUs5Q2pSUDZ4QiRQS1U4KnNy')
    request.add_header('Cache-Control', 'no-cache')
    request.get_method = lambda: 'PUT'
    url = opener.open(request)

    print url.read()
    logging.info(url.read())

def init_schedulers():
    #Class schedule start times for Mondays in Winter 2018
    #OccupiedDefault state set at ~10 minutes before class start time of each Monday
    schedule.every().monday.at("7:50").do(setOccupiedDefault)       #MAT 021A Calculus
    schedule.every().monday.at("8:50").do(setOccupiedDefault)       #ECN 101 Intermed Macro Theory
    schedule.every().monday.at("9:50").do(setOccupiedDefault)       #MAT 016A Short Calculus
    schedule.every().monday.at("10:50").do(setOccupiedDefault)      #MAT 017A Calculus for BioSci
    schedule.every().monday.at("12:00").do(setOccupiedDefault)      #MAT 016A Short Calculus
    schedule.every().monday.at("13:00").do(setOccupiedDefault)      #MAT 016A Short Calculus
    schedule.every().monday.at("14:00").do(setOccupiedDefault)      #MAT 021B Calculus
    schedule.every().monday.at("15:00").do(setOccupiedDefault)      #MAT 022A Linear Algebra
    schedule.every().monday.at("16:00").do(setOccupiedDefault)      #MAT 022B Differential Equations
    schedule.every().monday.at("17:00").do(setOccupiedDefault)      #MAT 021B Calculus
    schedule.every().monday.at("18:00").do(setOccupiedDefault)      #MAT 017A Calculus for BioSci

    #####################################
    #Class schedule start times for Tuesdays in Winter 2018
    #OccupiedDefault state set at ~10 minutes before class start time of each Tuesday
    schedule.every().tuesday.at("8:50").do(setOccupiedDefault)       #ENG 006 Engineering Problem Solving
    schedule.every().tuesday.at("10:20").do(setOccupiedDefault)      #MCB 121 Advanced Molecular Biology
    schedule.every().tuesday.at("10:20").do(setOccupiedDefault)      #MCB 121 Advanced Molecular Biology
    schedule.every().tuesday.at("12:00").do(setOccupiedDefault)      #GEL 012 Dinosaurs
    schedule.every().tuesday.at("13:30").do(setOccupiedDefault)      #ENG 190 Prof Resp of Engr
    schedule.every().tuesday.at("15:00").do(setOccupiedDefault)      #BIS 102 Struc-Func of Biomolecules

    #####################################
    #Class schedule end times for Wednesdays in Winter 2018
    #OccupiedDefault state set at ~10 minutes before class start time of each Wednesday
    schedule.every().wednesday.at("7:50").do(setOccupiedDefault)       #MAT 021A Calculus
    schedule.every().wednesday.at("8:50").do(setOccupiedDefault)       #ECN 101 Intermed Macro Theory
    schedule.every().wednesday.at("9:50").do(setOccupiedDefault)       #MAT 016A Short Calculus
    schedule.every().wednesday.at("10:50").do(setOccupiedDefault)      #MAT 017A Calculus for BioSci
    schedule.every().wednesday.at("12:00").do(setOccupiedDefault)      #MAT 016A Short Calculus
    schedule.every().wednesday.at("13:00").do(setOccupiedDefault)      #MAT 016A Short Calculus
    schedule.every().wednesday.at("14:00").do(setOccupiedDefault)      #MAT 021B Calculus
    schedule.every().wednesday.at("15:00").do(setOccupiedDefault)      #MAT 022A Linear Algebra
    schedule.every().wednesday.at("16:00").do(setOccupiedDefault)      #MAT 022B Differential Equations
    schedule.every().wednesday.at("17:00").do(setOccupiedDefault)      #MAT 021B Calculus
    schedule.every().wednesday.at("18:00").do(setOccupiedDefault)      #MAT 017A Calculus for BioSci

    #####################################
    #Class schedule end times for Thursdays in Winter 2018
    #OccupiedDefault state set at ~10 minutes before class start time of each Thursday
    schedule.every().thursday.at("8:50").do(setOccupiedDefault)       #ENG 006 Engineering Problem Solving
    schedule.every().thursday.at("10:20").do(setOccupiedDefault)      #MCB 121 Advanced Molecular Biology
    schedule.every().thursday.at("10:20").do(setOccupiedDefault)      #MCB 121 Advanced Molecular Biology
    schedule.every().thursday.at("12:00").do(setOccupiedDefault)      #GEL 012 Dinosaurs
    schedule.every().thursday.at("13:30").do(setOccupiedDefault)      #ENG 190 Prof Resp of Engr
    schedule.every().thursday.at("15:00").do(setOccupiedDefault)      #BIS 102 Struc-Func of Biomolecules

    #####################################
    #Class schedule start times for fridays in Winter 2018
    #OccupiedDefault state set at ~10 minutes before class start time of each friday
    schedule.every().friday.at("7:50").do(setOccupiedDefault)       #MAT 021A Calculus
    schedule.every().friday.at("8:50").do(setOccupiedDefault)       #ECN 101 Intermed Macro Theory
    schedule.every().friday.at("9:50").do(setOccupiedDefault)       #MAT 016A Short Calculus
    schedule.every().friday.at("10:50").do(setOccupiedDefault)      #MAT 017A Calculus for BioSci
    schedule.every().friday.at("12:00").do(setOccupiedDefault)      #MAT 016A Short Calculus
    schedule.every().friday.at("13:00").do(setOccupiedDefault)      #MAT 016A Short Calculus
    schedule.every().friday.at("14:00").do(setOccupiedDefault)      #MAT 021B Calculus
    schedule.every().friday.at("15:00").do(setOccupiedDefault)      #MAT 022A Linear Algebra
    schedule.every().friday.at("16:00").do(setOccupiedDefault)      #MAT 022B Differential Equations
    schedule.every().friday.at("17:00").do(setOccupiedDefault)      #MAT 021B Calculus
    schedule.every().friday.at("18:00").do(setOccupiedDefault)      #MAT 017A Calculus for BioSci

    #####################################
    #Class schedule end times for Fall 2017
    #Unoccupied state set at 10 minutes after last class of each weekday
    schedule.every().monday.at("19:10").do(setUnoccupied)
    schedule.every().tuesday.at("16:40").do(setUnoccupied)
    schedule.every().wednesday.at("19:10").do(setUnoccupied)
    schedule.every().thursday.at("16:40").do(setUnoccupied)
    schedule.every().friday.at("19:10").do(setUnoccupied)

    schedule.every().day.at("0:00").do(checkExperimentSchedule)     #check experiment schedule everyday at midnight

def run_schedule():
    while 1:
        schedule.run_pending()
        time.sleep(1)


#####################################
#Execute this code when program loads
while True:
    if __name__ == '__main__':
        init_schedulers()
        t = Thread(target=run_schedule)
        t.daemon = True
        t.start()

        checkExperimentSchedule()
        setOccupiedDefault()

        socketio.run(app, host='0.0.0.0',port=5000)
