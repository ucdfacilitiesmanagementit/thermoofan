#####################################
#SQLite Import and variables
import sqlite3
sqlite_file = 'thermoofan_db.sqlite'

#####################################
# Connecting to the database file
conn = sqlite3.connect(sqlite_file)
c = conn.cursor()

#####################################
# Create table
c.execute('''CREATE TABLE giedt1001
             (timeStamp text, fanVolts real, manualOverride integer)''')

#####################################
# Commit changes
conn.commit()

#####################################
# Close connection
conn.close()
