"""
Name: LJTickDAC
Desc: A modification of Labjack's LJTickDAC.py that removes the gui interface.
"""
import struct
import sys
import time

# Attempt to load the labjack driver
try:
    import LabJackPython
    import u3
    import u6
    import ue9
except:
    print("Driver error", '''The driver could not be imported. Please install the UD driver (Windows) or Exodriver (Linux and Mac OS X) from www.labjack.com''')
    sys.exit(1)


def toDouble(buffer):
    """
    Name: toDouble(buffer)
    Args: buffer, an array with 8 bytes
    Desc: Converts the 8 byte array into a floating point number.
    """
    right, left = struct.unpack("<Ii", struct.pack("B" * 8, *buffer[0:8]))

    return float(left) + float(right)/(2**32)


class LJTickDAC():
    """
    Name: LJTickDAC
    Desc: Uses of the I2C and LabJack Python modules to set the value of DACA and DACB in a LJ-TickDAC
    """
    U3 = 3
    U6 = 6
    UE9 = 9
    AUTO = 0
    U3_DAC_PIN_OFFSET = 4
    EEPROM_ADDRESS = 0x50
    DAC_ADDRESS = 0x12

    def __init__(self):

        self.VOLT_A = 0
        self.VOLT_B = 0
        self.override = False
        
        # Specify DAC Pins (U3: 0 for FIO 4/5 or 2 for FIO6/7)
        self.dacPin = 0
        
        # Load the devices
        self.loadFirstDevice()
                               
    def updateDevice(self):
        """
        Name: updateDevice()
        Desc: Changes DACA and DACB to the amounts specified by the user
         """
        # Determine pin numbers
        if self.deviceType == self.U3:
            sclPin = self.dacPin + LJTickDAC.U3_DAC_PIN_OFFSET
            sdaPin= sclPin + 1

        else:
            sclPin = self.dacPin
            sdaPin = sclPin + 1

        # Get voltage for DACA
        try:voltageA = float(self.VOLT_A)
        except:
            print("Invalid entry", "Please enter a numerical value for DAC A")
            return

        # self.Get voltage DACB
        try:voltageB = float(self.VOLT_B)
        except:
            print("Invalid entry", "Please enter a numerical value for DAC B")
            return

        # Make requests
        try:
            self.device.i2c(LJTickDAC.DAC_ADDRESS, [48, int(((voltageA*self.aSlope)+self.aOffset)/256), int(((voltageA*self.aSlope)+self.aOffset) % 256)], SDAPinNum=sdaPin, SCLPinNum=sclPin)
            self.device.i2c(LJTickDAC.DAC_ADDRESS, [49, int(((voltageB*self.bSlope)+self.bOffset)/256), int(((voltageB*self.bSlope)+self.bOffset) % 256)], SDAPinNum=sdaPin, SCLPinNum=sclPin)
        except:
            print("I2C Error", "Whoops! Something went wrong when setting the LJTickDAC. Is the device detached?\n\nPython error:" + str(sys.exc_info()[1]))
            sys.exit(1)

    def searchForDevices(self):
        """
        Name: searchForDevices()
        Desc: Determines which devices are available
        """
        self.u3Available = len(LabJackPython.listAll(LJTickDAC.U3)) > 0
        self.u6Available = len(LabJackPython.listAll(LJTickDAC.U6)) > 0
        self.ue9Available = len(LabJackPython.listAll(LJTickDAC.UE9)) > 0

    def loadFirstDevice(self):
        """
        Name: loadFirstDevice()
        Desc: Determines which devices are available and loads the first one found
        """
        try:
            self.searchForDevices()

            # Determine which device to use
            if self.u3Available:
                self.deviceType = LJTickDAC.U3
            elif self.u6Available:
                self.deviceType = LJTickDAC.U6
            elif self.ue9Available:
                self.deviceType = LJTickDAC.UE9
            else:
                print("Fatal Error", "No LabJacks were found to be connected to your computer.\nPlease check your wiring and try again.")
                sys.exit()

            self.loadDevice(self.deviceType)
        except:
            print("Fatal Error - First Load", "Python error:" + str(sys.exc_info()[1]))
            sys.exit()

    def loadDevice(self, deviceType):
        """
        Name: loadDevice(deviceType)
        Desc: loads the first device of device type
        """
        self.deviceType = deviceType

        # Determine which device to use
        if self.deviceType == LJTickDAC.U3:
            self.device = u3.U3()
        elif self.deviceType == LJTickDAC.U6:
            self.device = u6.U6()
        else:
            self.device = ue9.UE9()

        # Configure pins if U3
        if self.deviceType == LJTickDAC.U3:
            self.device.configIO(FIOAnalog=15, TimerCounterPinOffset=8)  # Configures FIO0-2 as analog

        # Get the calibration constants from the device
        self.device.getCalibrationData()

        # Get the calibration constants from the LJTick-DAC
        self.getCalConstants()

    def getCalConstants(self):
        """
        Name: getCalConstants()
        Desc: Loads or reloads the calibration constants for the LJTic-DAC
              See datasheet for more info
        """
        # Determine pin numbers
        if self.deviceType == self.U3:
            sclPin = self.dacPin + LJTickDAC.U3_DAC_PIN_OFFSET
            sdaPin= sclPin + 1

        else:
            sclPin = self.dacPin
            sdaPin = sclPin + 1

        # Make request
        data = self.device.i2c(LJTickDAC.EEPROM_ADDRESS, [64], NumI2CBytesToReceive=36, SDAPinNum=sdaPin, SCLPinNum=sclPin)
        response = data['I2CBytes']
        self.aSlope = toDouble(response[0:8])
        self.aOffset = toDouble(response[8:16])
        self.bSlope = toDouble(response[16:24])
        self.bOffset = toDouble(response[24:32])

        if 255 in response:
            print ("Pins", "The calibration constants seem a little off. Please go into settings and make sure the pin numbers are correct and that the LJTickDAC is properly attached.")

