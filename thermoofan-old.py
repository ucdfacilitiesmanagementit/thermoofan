#####################################
#Flask and Flask-SocketIO Imports
from flask import Flask, render_template
from flask_socketio import SocketIO, emit


#####################################
#LabJack Import
import LJTickDAC as lj


#####################################
#RepeatedTimer class import
from RepeatedTimer import RepeatedTimer


#####################################
#ControlVars class import
from ControlVars import control_vars


#####################################
#Class / variables to track slider voltage
class slider_voltage:
    def __init__(self):
        self.volt = 0

slider_v = slider_voltage()
labjack = lj.LJTickDAC()


#####################################
#initialize Flask webserver and Websocket layer
app = Flask(__name__)
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template('index.html', **values)


#####################################
#Values object to track slider value
values = {
    'slider1': 50
}


#####################################
#Control variables
cv = control_vars(10, 10, 10, 10, 10)


#####################################
#Override timer functionality
def reset_override(labjack):
    print "The override has ended."
    labjack.override = False
    update_lj(labjack, cv)
    override_timer.stop()

#Declare timer and stop it immediately
override_timer = RepeatedTimer(5, reset_override, labjack)
override_timer.stop()


#####################################
#Thermoostat_query function to reset to 5.0
def thermoostat_query(cv):
    therm_v = float(5)
    return therm_v


#####################################
#update_lj function to change LabJack voltage and send new voltage back to browser
def update_lj(labjack, cv):
    if labjack.override == False:
        therm_v = thermoostat_query(cv)
        if therm_v != labjack.VOLT_A:
            labjack.VOLT_A = therm_v
            labjack.VOLT_B = therm_v
            labjack.updateDevice()

        message = {
            'who': 'slider1',
            'data': int(labjack.VOLT_A*10)
        }
        #Emit 'update value' event to all connected browsers
        socketio.emit('update value', message, broadcast=True)
        print 'message sent'

        print "The voltage (non-overridden) updated to: %.2fv." % labjack.VOLT_A

    elif labjack.override == True:
        if slider_v.volt != labjack.VOLT_A:
            labjack.VOLT_A = slider_v.volt
            labjack.VOLT_B = slider_v.volt
            labjack.updateDevice()

        message = {
            'who': 'slider1',
            'data': int(labjack.VOLT_A*10)
        }
        socketio.emit('update value', message, broadcast=True)
        print 'message sent'

        print "The voltage (overridden) updated to: %.2fv." % labjack.VOLT_A


#####################################
#Event listner for socket event "value changed" sent from browser
@socketio.on('value changed')
def value_changed(message):
    if labjack.override == False:
        print "case 1"
        print labjack.VOLT_A
        override_timer.start()
    elif labjack.override == True:
        print "case 2"
        override_timer.stop()
        override_timer.start()

    values[message['who']] = message['data']
    #Emit 'update value' event to all connected browsers
    emit('update value', message, broadcast=True)

    labjack.override = True
    slider_v.volt = float(message['data'])/10

    update_lj(labjack, cv)


#####################################
#Execute this code when program loads
if __name__ == '__main__':
    update_lj(labjack, cv)
    socketio.run(app, host='0.0.0.0',port=5000) # NOTE: this line must FOLLOW voltage_timer or it will block
