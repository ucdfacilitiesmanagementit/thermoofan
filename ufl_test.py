# import http.client
import datetime as dt
import ssl
import urllib2

timestamp = '{:%Y-%m-%d %H:%M:%S}'.format(dt.datetime.now())
testFanSpeed = '3.0'
testManualOverrideIndicator = '0'

print "Sending new fan speed to PI UFL Connector..."

ssl._create_default_https_context = ssl._create_unverified_context

opener = urllib2.build_opener(urllib2.HTTPHandler)
request = urllib2.Request('https://util-cs-iis.ou.ad3.ucdavis.edu:5460/connectordata/GiedtFan/', data=timestamp+', Giedt1001-Ceiling-Fan, '+testFanSpeed+', '+testManualOverrideIndicator)
request.add_header('Authorization', 'Basic dWZsY29ubmVjdG9yOmNRJUs5Q2pSUDZ4QiRQS1U4KnNy')
request.add_header('Cache-Control', 'no-cache')
request.get_method = lambda: 'PUT'
url = opener.open(request)

print timestamp+', Giedt1001-Ceiling-Fan, '+testFanSpeed+', '+testManualOverrideIndicator
print url.read()
