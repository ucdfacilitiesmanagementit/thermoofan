#####################################
#Flask and Flask-SocketIO Imports
from flask import Flask, render_template
from flask_socketio import SocketIO, emit



#####################################
#Class / variables to track slider voltage
class slider_voltage:
    def __init__(self):
        self.volt = 0

slider_v = slider_voltage()


#####################################
#initialize Flask webserver and Websocket layer
app = Flask(__name__)
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template('index.html', **values)


#####################################
#Values object to track slider value
values = {
    'slider1': 50
}


#####################################
#Thermoostat_query function to reset to 5.0
def thermoostat_query(cv):
    therm_v = float(5)
    return therm_v


# #####################################
# #update_lj function to change LabJack voltage and send new voltage back to browser
# def update_lj(labjack, cv):
#     message = {
#         'who': 'slider1',
#         'data': int(sliver_v.volt*10)
#     }
#     socketio.emit('update value', message, broadcast=True)


#####################################
#Event listner for socket event "value changed" sent from browser
@socketio.on('value changed')
def value_changed(message):
    slider_v.volt = float(message['data'])/10
    # update_lj(labjack, cv)

    values[message['who']] = message['data']
    #Emit 'update value' event to all connected browsers
    emit('update value', message, broadcast=True)


#####################################
#Execute this code when program loads
if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0',port=5000) # NOTE: this line must FOLLOW voltage_timer or it will block
