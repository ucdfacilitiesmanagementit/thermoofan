# TherMOOfan

## Introduction

> A Python-based project to programmatically control ceiling fan speed based on TherMOOstat votes. Designed to run on a Raspberry Pi connected to a LabJack U3 DAQ and LJTick DAC. Uses a web-based interface via Flask.

## Installation

Requirements:
* Python 2.7
* LabJack Exodriver for Linux (https://labjack.com/support/software/installers/exodriver)
* LabJackPython (https://labjack.com/support/software/examples/ud/labjackpython)
* Flask-SocketIO (https://flask-socketio.readthedocs.io/en/latest/)

First, install the LabJack dependencies:

```sh
$ sudo apt-get install build-essential
$ sudo apt-get install libusb-1.0-0-dev
$ git clone git://github.com/labjack/exodriver.git
$ cd exodriver/
$ sudo ./install.sh
$ cd ..
$ git clone git://github.com/labjack/LabJackPython.git
$ cd LabJackPython/
$ sudo python setup.py install
$ cd ..
```

Then install Flask-SocketIO, python-dateutil, and schedule via `pip`:

```sh
$ sudo pip install flask-socketio
$ sudo pip install python-dateutil
$ sudo pip install schedule
```
Make sure the LabJack U3 DAQ with LJTick DAC are connected to the Raspberry Pi via USB.

Then, clone this repository and run 1) the `db-init.py` script to initialize the sqlite database, and 2) the `thermoofan.py` script to start the program:

```sh
$ git clone https://afs-dev.ucdavis.edu/stash/scm/cefs/thermoofan.git
$ cd thermoofan
$ python db-init.py
$ python thermoofan.py
```
You can also set this Python script to run automatically at startup by following these instructions: http://www.instructables.com/id/Raspberry-Pi-Launch-Python-script-on-startup/


For testing purposes, you can run this software in "dev" mode. This will use our dev-thermoostat server and API instead of the production one. Then, test your votes out on dev-thermoostat.ucdavis.edu.

To run in "dev" mode, just type:

```sh
$ python thermoofan.py --dev
```
