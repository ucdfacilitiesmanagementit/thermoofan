#####################################
#Flask and Flask-SocketIO Imports
from flask import Flask, render_template
from flask_socketio import SocketIO, emit


#####################################
#LabJack Import
import LJTickDAC as lj


#####################################
#Datetime Import
import datetime


#####################################
#SQLite Import and variables
import sqlite3
sqlite_file = 'thermoofan_db.sqlite'
db_table = 'giedt1001'
field1name = 'timeStamp'
field1type = 'TEXT' # e.g. YYY-MM-DD HH:MM:SS.SSS
field2name = 'fanVolts'
field2type = 'REAL' # e.g. 2.5
field3name = 'manualOverride'
field3type = 'INTEGER' # 0 for false, 1 for true


#####################################
#ControlVars class import
from ControlVars import control_vars


#####################################
#Class / variables to track slider voltage
class slider_voltage:
    def __init__(self):
        self.volt = 0

slider_v = slider_voltage()
labjack = lj.LJTickDAC()


#####################################
#initialize Flask webserver and Websocket layer
app = Flask(__name__)
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template('index.html', **values)


#####################################
#Values object to track slider value
values = {
    'value1': 50
}


#####################################
#Control variables
cv = control_vars(10, 10, 10, 10, 10)


#####################################
#Thermoostat_query function to reset to 5.0
def thermoostat_query(cv):
    therm_v = float(5)
    return therm_v


#####################################
#update_lj function to change LabJack voltage and send new voltage back to browser
def update_lj(labjack, cv):
    if slider_v.volt != labjack.VOLT_A:
        labjack.VOLT_A = slider_v.volt
        labjack.VOLT_B = slider_v.volt
        labjack.updateDevice()

    message = {
        'who': 'value1',
        'data': int(labjack.VOLT_A*10)
    }
    socketio.emit('update value', message, broadcast=True)
    print 'message sent'

    print "The voltage (overridden) updated to: %.2fv." % labjack.VOLT_A


#####################################
#Event listner for socket event "value changed" sent from browser
@socketio.on('value changed')
def value_changed(message):
    slider_v.volt = float(message['data'])/10
    update_lj(labjack, cv)

    values[message['who']] = message['data']
    #Emit 'update value' event to all connected browsers
    emit('update value', message, broadcast=True)

    timestamp = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
    override = 0 # 'false'
    insert_row((timestamp,slider_v.volt,override))

#####################################
#Database row insert function
def insert_row(values):
    # Connecting to the database file
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    # Insert the row
    c.execute("INSERT INTO giedt1001 VALUES (?,?,?)", values)
    # Commit changes and close the connection
    conn.commit()
    conn.close()


#####################################
#Execute this code when program loads
if __name__ == '__main__':
    update_lj(labjack, cv)
    socketio.run(app, host='0.0.0.0',port=5000) # NOTE: this line must FOLLOW voltage_timer or it will block
