#####################################
#Datetime Utilities
import datetime as dt
import dateutil.parser  #for parsing ISO 8601 timestamps from Thermoostat API
import calendar
todaysDate = None

todaysExperimentSchedule = None

baselineDates = frozenset([     #only for Tuesdays/Thursdays
    '09-28-2017',
    '10-03-2017',
    '10-12-2017',
    '10-17-2017',
    '10-26-2017',
    '10-31-2017',
    '11-09-2017'
])

treatment1Dates = frozenset([   #only for Tuesdays/Thursdays
    '10-05-2017',
    '10-10-2017',
    '10-19-2017',
    '10-24-2017',
    '11-02-2017',
    '11-07-2017'
])


def checkExperimentSchedule():
    todaysDate = dt.datetime.today().strftime("%m-%d-%Y")
    print 'todaysDate: '
    print todaysDate
    todaysDay = calendar.day_name[dt.datetime.today().weekday()]
    print 'todaysDay: '
    print todaysDay

    if todaysDay == 'Saturday' or todaysDay == 'Sunday':
        todaysExperimentSchedule = 'baseline'
    elif todaysDay == 'Monday' or todaysDay == 'Wednesday' or todaysDay == 'Friday':
        if todaysDate == '09-27-2017' or todaysDate == '09-29-2017':
            todaysExperimentSchedule = 'baseline'
        else:
            todaysExperimentSchedule = 'treatment1'   #always treatment1 until "votes stabilize", then code will need to be modified here
    else: #todaysDay == 'Tuesday' or todaysDay == 'Thursday'
        if todaysDate in baselineDates:
            todaysExperimentSchedule = 'baseline'
        elif todaysDate in treatment1Dates:
            todaysExperimentSchedule = 'treatment1'
        else:    #treatment2
            todaysExperimentSchedule = 'treatment2'

    print todaysExperimentSchedule

checkExperimentSchedule()   #always check on program init
