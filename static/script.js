$(document).ready(function() {

  var socket = io.connect();

  var firstClick = function() {
    $('#progress-bar').removeClass();
    $('#progress-bar').addClass('first');
    $('span').removeClass('bold');
    $('div.first > span').addClass('bold');
    $('span.second').removeClass('border-change');
    $('span.third').removeClass('border-change');
    $('span.fourth').removeClass('border-change');

    $('img').removeClass();
    $('#off').addClass('visible');

    $('.speed').text('OFF');
  };

  var secondClick = function() {
    $('span.third').removeClass('border-change');
    $('span.fourth').removeClass('border-change');
    $('#progress-bar').removeClass();
    $('#progress-bar').addClass('second');
    $('span').removeClass('bold');
    $('div.second > span').addClass('bold');
    $('span.second').addClass('border-change');
    $('span.first').addClass('border-change');

    $('img').removeClass();
    $('#low').addClass('visible');

    $('.speed').text('LOW');
  };

  var thirdClick = function() {
    $('span.fourth').removeClass('border-change');
    $('#progress-bar').removeClass();
    $('#progress-bar').addClass('third');
    $('span').removeClass('bold');
    $('div.third > span').addClass('bold');
    $('span.third').addClass('border-change');
    $('span.second').addClass('border-change');
    $('span.first').addClass('border-change');

    $('img').removeClass();
    $('#med').addClass('visible');

    $('.speed').text('MEDIUM');
  };

  var fourthClick = function() {
    $('#progress-bar').removeClass();
    $('#progress-bar').addClass('fourth');
    $('span').removeClass('bold');
    $('div.fourth > span').addClass('bold');
    $('span.progress-span').addClass('border-change');

    $('img').removeClass();
    $('#high').addClass('visible');

    $('.speed').text('HIGH');
  };

  var updateVoltDisplay = function(value) {
    var voltDisplay = value/10;
    $('.voltage-indicator').text(voltDisplay+"V");
  }

  var socketEmit = function(value) {
    socket.emit('value changed', {
      who: 'value1',
      data: value
    });
    updateVoltDisplay(value);
  }

  var fillBar = function(value) {
    $('#progress-bar').val(value);

    if(value === 0) {
      firstClick();
    }else if(value <= 11) {
      secondClick();
    }else if(value <= 16) {
      thirdClick();
    }else {
      fourthClick();
    }
  }

  $('div.first').click(function() {
    firstClick();
    fillBar(0);
    socketEmit(0);
  });
  $('div.second').click(function() {
    secondClick();
    fillBar(11);
    socketEmit(11);
  });
  $('div.third').click(function() {
    thirdClick();
    fillBar(16);
    socketEmit(16);
  });
  $('div.fourth').click(function() {
    fourthClick();
    fillBar(21);
    socketEmit(21);
  });

  var barLength = $('#progress-bar').width();
  var barOffset = $('#progress-bar').offset();
  $('#progress-bar').click(function(event) {
    var x = event.pageX - barOffset.left;
    var progressFill = Math.round((x / barLength)*21);
    if(progressFill < 11 && progressFill > 0) {
      progressFill = 11;   //minimum value must be 10 if not 'off'
    }
    fillBar(progressFill);
    socketEmit(progressFill);
  });

  socket.on('update value', function(msg) {
    fillBar(msg.data);
    updateVoltDisplay(msg.data);
  });

});
