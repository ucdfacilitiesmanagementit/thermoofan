#####################################
#Datetime Utilities
import datetime as dt
import dateutil.parser  #for parsing ISO 8601 timestamps from Thermoostat API
import dateutil.tz      #for adding timezone to datetime objects

#####################################
#urllib2 & json libraries for parsing Thermoostat API in thermoostat_query function
import urllib2
import json
opener = urllib2.build_opener()

#####################################
#Thermoostat_query function to return JSON of today's data from Thermoostat API
def thermoostat_today_query():
    apiKey = 'ET9wgnDTT546jGaN'
    todaysDate = dt.datetime.today().strftime("%m-%d-%Y")

    req = urllib2.Request("https://thermoostat.ucdavis.edu/api/thermoo/"+apiKey+"/date/"+todaysDate)
    #Uncomment the following line and comment previous one to test whole building results over all time (good for testing when today's date has limited results)
    # req = urllib2.Request("https://thermoostat.ucdavis.edu/api/thermoo/"+apiKey+"/building/Giedt%20Hall")
    f = opener.open(req)
    jsonFile = json.loads(f.read())
    return jsonFile
    opener.close()

query = thermoostat_today_query()

giedtBuildingFeedbacks = []
giedtRoom1001Feedbacks = []
offsetIntervalFeedbacks = []

datetimeNow = dt.datetime.now(dateutil.tz.tzutc())    #today's datetime in UTC timezone to match thermoostat api
offsetIntervalMinutes = 1;  #pull last 1 minute of data
datetimeNowMinusOffset = datetimeNow - dt.timedelta(minutes=1)

for obj in query:
    for key, value in obj.items():
        if key == 'building':
            if value == 'Giedt Hall':
                giedtBuildingFeedbacks.append(obj)

for obj in giedtBuildingFeedbacks:
    for key, value in obj.items():
        if key == 'room':
            if value == '1001':
                giedtRoom1001Feedbacks.append(obj)

for obj in giedtRoom1001Feedbacks:
    for key, value in obj.items():
        if key == 'timestamp':
            datetime = dateutil.parser.parse(value)
            if datetime > datetimeNowMinusOffset:
                offsetIntervalFeedbacks.append(obj)

print offsetIntervalFeedbacks
